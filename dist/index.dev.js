"use strict";

var admin = require('firebase-admin');

var serviceAccount = require('./admin.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://test-node-project-4a415-default-rtdb.firebaseio.com"
});
var db = admin.database();
var userRef = db.ref("users");

function writeUserData(userId, name, email) {
  var userListRef = admin.database().ref('users');
  var newUserRef = userListRef.push();
  newUserRef.set({
    name: name,
    email: email
  }); // admin.database().ref('users/' + userId).set({
  //     name: name,
  //     email: email,
  // });
} // writeUserData("1", "jaykishan", 'fdfsdf', 'fdfsdf');
// addUser(obj, res) {
//         var oneUser = userRef.child(obj.roll);
//         oneUser.update(obj, (err) => {
//             if (err) {
//                 res.status(300).json({ "msg": "Something went wrong", "error": err });
//             } else {
//                 res.status(200).json({ "msg": "user created sucessfully" });
//             }
//         })
//     }
// const http = require('http');
// const hostname = '127.0.0.1';
// const port = 3000;
// const server = http.createServer((req, res) => {
//     res.statusCode = 200;
//     res.setHeader('Content-Type', 'text/plain');
//     res.end('Hello World');
//     writeUserData("2", "jaykishan", 'fdfsdf', 'fdfsdf');
// });
// server.listen(port, hostname, () => {
//     console.log(`Server running at http://${hostname}:${port}/`);
// });


var express = require('express');

var app = express();

var fs = require("fs");

app.use(express.json());
user = {
  "user4": {
    "name": "mohit",
    "email": "email",
    "id": 4
  }
};
app.get('/listUsers', function (req, res) {
  var users = admin.database().ref('users');
  users.on('value', function (snapshot) {
    var data = snapshot.val();
    res.send(data);
  });
});
app.post('/addUser', function (req, res) {
  var already_exists = false; // console.log(req.body.id);

  admin.database().ref("users").orderByChild("email").equalTo(req.body.email).on('value', function (snapshot) {
    if (snapshot.exists()) {
      console.log("hi"); // res.send(snapshot.val());

      already_exists = true;
      return res.send("Email already added");
    }
  });

  if (!already_exists) {
    console.log("No data available");
    writeUserData(req.body.id, req.body.name, req.body.email);
    return res.send("user added");
  }
});
app.put('/updateUser', function (req, res) {
  console.log(req.body.id);
  admin.database().ref("/users/" + req.body.id).once('value').then(function (snapshot) {
    if (snapshot.exists()) {
      admin.database().ref('users/' + req.body.id).set({
        name: req.body.name,
        email: req.body.email
      });
      res.send("updated success");
    } else {
      console.log("No data available");
    }
  })["catch"](function (error) {
    console.error(error);
  });
});
app.get('/:id', function (req, res) {
  // First read existing users.
  var users = admin.database().ref('users/' + req.params.id);
  users.on('value', function (snapshot) {
    var data = snapshot.val();
    res.send(data);
  });
});
app["delete"]('/deleteUser/:id', function (req, res) {
  var users = admin.database().ref('users/' + req.params.id).remove();
  res.send("delete successfull!");
});
var server = app.listen(8081, function () {
  var host = server.address().address;
  var port = server.address().port; // console.log(app);

  console.log("Example app listening at http://%s:%s", host, port);
});